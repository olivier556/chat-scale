const { MongoClient } = require("mongodb")

module.exports = async function mongo() {
  const client = new MongoClient('mongodb://localhost:27017')
  const db = await client.connect()
  const dbo = db.db('chat')

  let batch = []

  setInterval(() => {
    if (batch.length === 0) return
    console.log('BATCH UPLOAD', batch.length)
    const copy = batch.slice()
    batch = []
    dbo.collection('messages').insertMany(copy)
  }, 1000)

  return {
    addMessage(message) {
      batch.push({
        message,
        date: Date.now()
      })
    }
  }
}
