const cluster = require("cluster")
const http = require("http")
const { Server } = require("socket.io")
const { setupMaster, setupWorker } = require("@socket.io/sticky")
const { createAdapter, setupPrimary } = require("@socket.io/cluster-adapter")
const mongo = require('./mongo')

const numCPUs = 8

if (cluster.isMaster) {
  startMaster()
} else {
  startWorker()
}

function startMaster () {
  console.log(`Master ${process.pid} is running`)

  const httpServer = http.createServer()

  setupMaster(httpServer, {
    loadBalancingMethod: 'least-connection',
  })
  setupPrimary()

  httpServer.listen(3000)

  for (let i = 0; i < numCPUs; i++) {
    cluster.fork()
  }

  cluster.on('exit', (worker) => {
    console.log(`Worker ${worker.process.pid} died`)
    cluster.fork()
  })
}

async function startWorker () {
  console.log(`Worker ${process.pid} started`)

  const httpServer = http.createServer()
  const io = new Server(httpServer)
  io.adapter(createAdapter())
  setupWorker(io)
  const db = await mongo()

  io.on('connection', socket => {
    socket.on('connect_error', (err) => {
      console.log(`connect_error due to ${err.message}`);
    });
    socket.on('message', message => {
      db.addMessage(message)
      socket.broadcast.emit('message', {
        message
      })
    })
  })
}