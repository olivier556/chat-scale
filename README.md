# Chat / Scaling Assignment

1. Make a chat based on socket io
2. Scale to work with 100K msgs per min
3. Insert messages in DB
4. Optimize DB for 100K writes per min

## Running it

1. `yarn install`
2. `brew services start mongodb`
3. `cd backend`
4. `node ./index.js`

## Load testing

1. `cd backend`
2. `node ./index.js`
3. `yarn run artillery` // load test script

## What and how it was done

- I didn't have time to implement a FE.
- Used node.js cluster to distribute the load across multiple threats
- Used Socket.io cluster module to handle distributing workload based on worker with  least connections
- Socket.io cluster module handles IPC across workers to emit to all connected sockets from any worker
- Used mongoDB because it's easy to iterate on
- Used artillery.io to load test. This took a good chunk of my CPU power which affected my local results.
- I tested everything on my local computer and peaked at 40k messages a minute with 3k concurrent users before reaching CPU bottlenecks.
- Using sticky sessions to account for long polling clients (websocket fallback, taken into account by load test)
- All "users" where connected to the same "chatroom" which increased the amount of data needed to be sent from server to client by quite a bit
- Did batch insertions on DB to improve performance

## Closing thoughts

- I wish i had more time to get this to the finish line but unfortunately i'm out of time!
- I tried to focus on the scalebility side of things as this seemed to be what the tasks focus was on.
- To take this to the next level, we'd use redis to not only distribute work across threads, but also computers.